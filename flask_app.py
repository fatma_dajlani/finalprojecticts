from flask import Flask, redirect, url_for, render_template, request, flash
from flask_login import LoginManager, UserMixin,  current_user, login_user, login_required, logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from pony.orm import Database, Required, Optional, db_session, select, PrimaryKey, Set
from datetime import datetime
#import os
#import secrets
#from PIL import Image
import keygen
from forms import LoginForm, PostForm




app = Flask(__name__)
app.secret_key = keygen.generate()
login = LoginManager(app)
login.login_view = 'login'

db = Database()

class User(UserMixin, db.Entity):

    id= PrimaryKey(int, auto= True)
    username = Required(str, unique=True)
    email = Required(str, unique=True)
    password_hash = Optional(str)
    image_file = Optional(str, default='default.png')
    posts=Set('Post')



    @db_session
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    @db_session
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @db_session
    def check_user(username):
        return User.exists(username=username)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"

class Post(db.Entity):

    id= PrimaryKey(int, auto= True)
    title= Required(str, unique=True)
    date_posted= Optional(datetime, default=datetime.utcnow)
    content= Required(str, unique=True)
    author= Required(str, unique=False)
    user_id= Optional('User')

    def __repr__(self):
        return f"Post('{self.title}', '{self.date_posted}')"

db.bind(provider='sqlite', filename='mydb', create_db=True)
db.generate_mapping(create_tables=True)

@login.user_loader
@db_session
def load_user(id):
    return User.get(id=int(id))


@app.route('/')
@db_session
def index():

    posts=select(p for p in Post)
    return render_template('index.html', posts=posts)

@app.route("/about")
def about():
    return render_template('about.html', title='About')

@app.route('/login', methods=['GET', 'POST'])
@db_session
def login():

    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.get(username=form.username.data)

        if user is None or not user.check_password(form.password.data):
            flash('Please check your username or password')
            return redirect(url_for('login'))

        login_user(user)
        return redirect(url_for('index'))

    return render_template('login.html', title='Sign In', form=form)

@app.route('/new_user', methods=['GET', 'POST'])
@db_session
def new_user_form():
    if request.method == 'GET':

        return render_template('newuserform.html')

    elif request.method == 'POST':
        data = request.form.to_dict()

        u = User(username=data['username'], email=data['email'])
        u.set_password(data['password'])
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))


@app.route('/logout')
@db_session
@login_required
def logout():
    logout_user()
    return redirect('/')
'''
def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)

    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn
'''

@app.route('/account', methods=['GET', 'POST'])
@db_session
@login_required
def account():
    return render_template('account.html')


@app.route('/account/<id>/', methods=['GET', 'POST'])
@db_session
def modify(id):
    if request.method == 'GET':
        if User[id]:
            return render_template('edit_account.html', current_user=User[id])

    elif request.method == 'POST':
        if User[id]:
            current_user.username = request.form.get('username')
            current_user.email = request.form.get('email')
            User[id].set(username=current_user.username, email=current_user.email)

            return redirect(url_for('index'))


@app.route("/post/new", methods=['GET', 'POST'])
@db_session
@login_required

def new_post():
    form = PostForm()
    if request.method == 'GET':
        return render_template('create_post.html', title='New Post', legend='Create Post', form=form)

    elif request.method == 'POST':

        title = request.form.get('title')
        content = request.form.get('content')


        Post(title=title, content=content, author= current_user.username)

        flash('Your post has been created!', 'success')
        return redirect(url_for('index'))

@app.route("/post/<id>", methods=['GET', 'POST'])
@db_session
def post(id):
    if request.method == 'GET':
        if Post[id]:
            return render_template('post.html', post=Post[id])

@app.route("/post/<id>/update", methods=['GET', 'POST'])
@db_session
@login_required
def update_post(id):
    '''
    post= select(p for p in Post if id==Post.id)
    if Post.author != current_user:
        abort(403)
  '''

    if request.method == 'GET':
        if Post[id]:
            return render_template('update_post.html', post=Post[id])

    elif request.method == 'POST':
        if Post[id]:
            title = request.form.get('title')
            content = request.form.get('content')
            Post[id].set(title=title, content=content)

            return redirect(url_for('index'))

@app.route("/post/<id>/delete", methods=['GET', 'POST'])
@db_session
@login_required
def delete(id):
    if Post.get(id=id):
        Post[id].delete()

    flash('Your post was deleted')
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(threaded=True, port=5000)

